/*Free implementation of string functions:
* strcmp,strncmp,strchr 
* from standart C string library string.h
* 
* Maman 11, question 1
* Written by Andrey Voroshnin
* 23/10/17
*/

#include <stdio.h>

/*Max string length (array of chars)*/
#define MAX_STR_LEN 128

/* Print menu and get input from user
@var input used to hold value of user input.
No check for error if non integer value was entered.
Because we can assume that all values are entered correctly
as per question definition */
int menu()
{
	int input=0;

	printf("Please choose function you want to use:\n");
	printf("1. my_strcmp to compare 2 strings.\n");
	printf("2. my_strncmp to compare first 'n' characters from 2 strings\n");
	printf("3. my_strchr to find specified character in string\n");
	printf("0. Exit\n");
	printf("Your choice (end with enter): ");
	scanf("%d", &input);

	return input;
}

/* Sub function to get string length
Wasn't sure if I can use string.h at all
Iterate thru string till '\0' end of string
and return number of iterations */
int my_strlen(const char* str1)
{
	int i=0;
	while (str1[i]!='\0')
		i++;

	return i;
}

/* String comparison char by char and comparing their ASCII value.
First compare string length. If they're unequal, no need to execute whole function
If equal, compare char by char and return their difference.
return positive value if str1 is bigger than str2
return negative value if str2 is bigger than str1
return 0 if strings are equal */
int my_strcmp(const char* str1, const char* str2)
{
	int str1_len,str2_len,i=0;
	/*Get string length*/
	str1_len=my_strlen(str1);
	str2_len=my_strlen(str2);
	/*Compare length*/
	if (str1_len!=str2_len)
		return str1_len-str2_len;
	/*iterate thru both strings and compare ASCII values*/
	while (str1[i]==str2[i] && str1[i]!='\0')
		i++;

	if (str1[i]=='\0')
		return 0;

	return str1[i]-str2[i];
}

/* Compare first 'n' number of characters in 2 strings
Comparison is done with ASCII values of characters
before making actual compare I compare if strings are equal
Only if strings are equal begin char by char compare
return positive value if str1 is bigger than str2
return negative value if str2 is bigger than str1
return 0 if strings are equal */
int my_strncmp(const char* str1, const char* str2, const int chr_num)
{
	int str1_len,str2_len,i=0;
	/*Get string length*/
	str1_len=my_strlen(str1);
	str2_len=my_strlen(str2);
	/*if requested 'n' characters are in bound with both strings*/
	if (chr_num>str1_len && chr_num<str2_len)
		/*str1 bigger than str2*/
		return str1_len-str2_len;
	if (chr_num<str1_len && chr_num>str2_len)
		/*str2 bigger than str1*/
		return str1_len-str2_len;
	/*Char by char compare till end of the strings or first 'n' characters*/
	while (i<chr_num-1 && str1[i]!='\0' && str2[i]!='\0')
	{
		if(str1[i]==str2[i])
			i++;
		else
			return str1[i]-str2[i];
	}
	
	return str1[i]-str2[i];
}

/* Find first occurence of specific character in string
return index in string where character is found
return -1 if char not found */
int my_strchr(const char* str1, const int chr_find)
{
	int str1_len,i=0;
	str1_len=my_strlen(str1);
	while(i!=str1_len)
	{
		if(str1[i]==chr_find)
			return i;
		else
			i++;
	}
	
	return -1;
}


/* char* my_strchr(const char* str1, const int chr_find)
{
	int str1_len,i=0;
	str1_len=my_strlen(str1);
	while(i!=str1_len)
	{
		if(str1[i]==chr_find)
			return (char*)(str1+i);
		else
			i++;
	}
	
	return NULL;
} */

 
int main(void)
{
	int output,input,chr_num=0;
	char chr_find;
	char str1[MAX_STR_LEN];
	char str2[MAX_STR_LEN];
	/*char* tmp;*/

	printf("Welcome to my_string implementation\n");
	/*Run till exit '0' is entered*/
	while ((input=menu())!=0)
	{
		/*Using switch case because of easier impelentation and readability*/
		/*In all cases no input check, asssuming all are correct*/
		/*Check is done in case. Could write different functions but it is easier to read like this*/
		/*Because of scanf no space or tab is allowed*/
		switch(input)
		{
			case 1:
				printf("Please enter first string:\n");
				scanf("%s",str1);
				printf("Please enter second string:\n");
				scanf("%s",str2);
				printf("\nRunning function my_strcmp.\n");
				output=my_strcmp(str1,str2);
				/*Pretty explanation for user*/
				printf("Function result is: \"%d\" and is meaning:\n",output);
				if (output==0)
					printf("Strings \"%s\" and \"%s\" are equal\n\n",str1,str2);
				else if (output>0)
					printf("String \"%s\" is bigger than \"%s\"\n\n",str1,str2);
				else
					printf("String \"%s\" is bigger than \"%s\"\n\n",str2,str1);
				break;
			case 2:
				printf("Please enter first string:\n");
				scanf("%s",str1);
				printf("Please enter second string:\n");
				scanf("%s",str2);
				printf("Please enter number of characters to compare: ");
				scanf(" %d", &chr_num);
				printf("\nRunning function my_strncmp.\n");
				output=my_strncmp(str1,str2,chr_num);
				printf("Function result is: \"%d\" for first \"%d\" characters and is meaning:\n",output,chr_num);
				if (output==0)
					printf("Strings \"%s\" and \"%s\" are equal\n\n",str1,str2);
				else if (output>0)
					printf("String \"%s\" is bigger than \"%s\"\n\n",str1,str2);
				else
					printf("String \"%s\" is bigger than \"%s\"\n\n",str2,str1);
				break;
			case 3:
				printf("Please enter your string:\n");
				scanf("%s",str1);
				printf("Please enter character to find: ");
				scanf(" %c", &chr_find);
				printf("\nRunning function my_strchr.\n");
				output=my_strchr(str1,chr_find);
				/*Print index+1 of char. More readability*/
				printf("Function result is: \"%d\" for string \"%s\" and is meaning:\n",output,str1);
				if (output!=-1)
					printf("Character \"%c\" was found at index %d or place %d in string \"%s\"\n\n",chr_find,output,output+1,str1);
				else
					printf("Character \"%c\" wasn't found in string \"%s\"\n\n",chr_find,str1);
				break;
			case 0:
				printf("Thank you and goodbye\n");
				break;
			default:
				printf("Unknown command. Please try again\n");
				break;
		}
	}

	return 1;
}